//Асинхронность в js это когда движок Джаваскрипта выполняет параллельно несколько и более операций, например, async/await, setTimeout


async function showInfo() {

    const response = await fetch('https://api.ipify.org/?format=json');
    const userIp = await response.json()
    const {
        ip: ipUnique
    } = userIp;

    const getIpUnique = await fetch(`http://ip-api.com/json/${ipUnique}?fields=status,continent,country,regionName,city,district`)
    const userInfo = await getIpUnique.json();

    return userInfo;

}

const button = document.getElementById("button");
showInfo().then(({
    continent,
    country,
    regionName,
    city,
    district
}) => {
    const ipInfo = new IpInfo(continent, country, regionName, city, district);
    button.addEventListener("click", (e) => {
        // e.preventDefault();
        ipInfo.render()
    })
})



class IpInfo {
    constructor(continent, country, regionName, city, district) {
        this.continent = continent,
            this.country = country,
            this.regionName = regionName,
            this.city = city,
            this.district = district
    }

    render() {
        button.insertAdjacentHTML('afterend', `<li>Continent : ${this.continent ? this.continent : " No such property"}</li><li>Country : ${this.country ? this.country : " No such property"}</li><li>Region Name : ${this.regionName ? this.regionName : " No such property"}</li><li>City : ${this.city ? this.city : " No such property"}</li><li>District :${this.district ? this.district : " No such property"}</li> `);
    }

}

